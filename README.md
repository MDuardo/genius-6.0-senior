# Concurso de programación Genius 6.0



Este repositorio contiene los ejercicios realizados junto con sus soluciones en
los idiomas de programación que fueron permitidos, el concurso de programación
se desarrolló el día 23 de mayo del 2024 en la UDI (Universidad de Investigación
y Desarrollo) en la categoría senior.

## Lenguajes de programación permitidos

- C
- C++
- Java
- Python

## Instrucciones

Todos los ejercicios pueden ser ejecutados usando uso de un compilador en línea,
el compilador [OnlineGDB](https://www.onlinegdb.com/) fue el compilador usado en
el concurso.
